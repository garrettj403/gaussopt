# Makefile for GaussOpt
#
# Makefile examples that I drew from:
# https://github.com/pjz/mhi/blob/master/Makefile
# http://krzysztofzuraw.com/blog/2016/makefiles-in-python-projects.html
#

TEST_PATH=./
GO_PATH=./


all: init update test clean


# Install pacakge ------------------------------------------------------------

init:
	python setup.py install


# Install requirements (with Pip) --------------------------------------------

install:
	pip install -r requirements.txt

upgrade:
	pip install -r requirements.txt --upgrade

update: upgrade


# Install requirements (with Conda) ------------------------------------------

install_with_conda:
	conda install --yes --file=requirements.txt

upgrade_with_conda:
	conda upgrade --yes --file=requirements.txt

update_with_conda: upgrade_with_conda


# Run tests (with Py.Test) ---------------------------------------------------

test: clean
	pytest --verbose --color=yes --cov=$(GO_PATH) --cov-report=html $(TEST_PATH) 
	open htmlcov/index.html

install_pytest:
	pip install pytest 
	pip install pytest-cov

clean_test: clean
	find . -name 'htmlcov' -exec rm -rf {} +


# Clean bytecode -------------------------------------------------------------

clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +


# Misc -----------------------------------------------------------------------

.PHONY: install_with_conda upgrade_with_conda install upgrade update test clean all
