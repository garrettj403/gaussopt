import gaussopt as go
import numpy as np
from scipy.constants import c as C0
GHz = 1e9


def test_gaussian_telescope():
    """For this test, a Gaussian telescope is created. I ensure that perfect
    coupling is found (as expected). The system is roughly based on my own 
    system. Focal length of all mirrors = 160mm."""

    start = 180*GHz
    stop  = 280*GHz
    npts  = 101
    frequency = np.linspace(start, stop, npts)
    ind_center = np.abs(frequency - 230*GHz).argmin()
    wavelength = C0 / frequency * 1000

    # Input horn
    q_in, waist_in, z_offset_in = go.horn(22.64, 3.6, 0.59, wavelength)

    # Distance between horn and mirror
    # Reduced to take into account the offset between the beam waist and the 
    # horn's aperture
    reduced_distance = 160 - z_offset_in[ind_center]

    # Optical components
    air_160_reduced = go.freespace(reduced_distance)
    air_160         = go.freespace(160)
    mirror_f160     = go.mirror(160)

    # Build system
    system = air_160_reduced * mirror_f160 * air_160 * air_160 * mirror_f160 * air_160_reduced

    # Output beam
    q_out, waist_out, R_out = go.beam_output(system, q_in, wavelength)

    # Coupling to a 'Gubbins' horn
    coupling = go.coupling(q_out, 22.64, 3.6, 0.59, wavelength)

    # Test coupling
    # Should be perfect for center frequency
    assert coupling[ind_center] == 1.

    # Should not be perfect for other frequencies
    # (beam waist offset changes in the horns)
    for i in range(npts):
        if i != ind_center:
            assert coupling[i] != 1.

    return frequency, coupling


def test_dielectric():
    """Make sure that adding an x amount of dielectric is equivalent to 
    adding x*n amount of air."""

    distance = 100  # mm
    index_of_refraction = 3

    air  = go.freespace(distance)
    diel = go.dielectric(distance, index_of_refraction)

    assert air[0, 1] * index_of_refraction == diel[0, 1]
    assert air[0, 0] == diel[0, 0]
    assert air[1, 0] == diel[1, 0]
    assert air[1, 1] == diel[1, 1]


def test_edge_taper():

    beam_waist = 30
    aperture_rad = beam_waist * 1.2

    # Check edge taper against table 2.1 in Goldsmith
    assert round(go.edge_taper(beam_waist, aperture_rad), 1) == 12.5
