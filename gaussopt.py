"""
**Module Description:** Module to analyze quasi-optical systems using Gaussian
beam analysis.

Quasi-optical analysis is important whenever the wavelength is comparable to
the size of the optical components (making diffraction very important but not 
dominant). Gaussian beam analysis of quasi-optical systems assumes that the 
transverse amplitude profile (the E- or H-field) of the beam is similar to a 
Gaussian function. This is roughly true for beams originating from waveguide 
horn antennas.

This module uses the matrix approach to Gaussian beam transformations. I used
"Quasioptical Systems" by P. Goldsmith as a reference. I reference some of the
equations from this book in my comments.

**Note:** 

   1. Be consistent with dimensions -- use either millimetres or metres 
      consistently!

   2. Wavelength can either be a float/int or a np.array.

"""

import numpy as np


# Optical components ----------------------------------------------------------

def mirror(focal_len):
    """
    Optical component. Gives optical matrix for a mirror.

    :param float focal_len: focal length
    :return: optical matrix for a mirror
    """

    opt_matrix = np.matrix([[1., 0.], [-1. / focal_len, 1.]])

    return opt_matrix


def freespace(dist):
    """
    Optical component. Gives optical matrix for freespace.

    :param float dist: distance (in mm)
    :return: optical matrix for freespace
    """

    opt_matrix = np.matrix([[1., dist], [0., 1.]])

    return opt_matrix


def dielectric(dist, n_diel):
    """
    Optical component. Gives optical matrix for a slab of dielectric.

    :param float dist: distance (in mm)
    :param float n_diel: index of refraction of the dielectric
    :return: optical matrix for a slab of dielectric
    """

    opt_matrix = np.matrix([[1., dist * n_diel], [0., 1.]])

    return opt_matrix


# Horn -----------------------------------------------------------------------


def horn(slen, arad, hf, wlen):
    """
    Calculate the beam parameters from the dimensions of a
    horn (using q_horn). This function can handle a frequency array.

    For example in our lab, 

       - the 'Gubbins' horn has: slen = 22.64, arad = 3.6, hf = 0.59
       - the 'corrugated' horn has: slen = 49.2, arad = 7.6, hf = 0.644

    where slen and arad are in mm.

    :param float slen: slant length (in mm)
    :param float arad: aperture radius (in mm)
    :param float hf: horn factor
    :param float wlen: wavelength (in mm)
    :return: beam parameter q, beam waist, z offset
    """

    # wlen can either be an array or a float
    if type(wlen) != np.ndarray:

        # Eqn 7.38a and 7.38b (note there is an error in the textbook)
        w = hf * arad / np.sqrt(1 + (np.pi * (hf * arad) ** 2 /
                                (wlen * slen)) ** 2)
        zoff = slen / (1 + (wlen * slen / (np.pi * (hf * arad) ** 2)) ** 2)
        q_waist = q_from_waist(w, wlen)

        qap = q_output(freespace(zoff), q_waist)

        return qap, w, zoff

    # A crappy hack, but this isn't a computer intensive package
    elif type(wlen) == np.ndarray:

        npts = np.alen(wlen)
        qap_arr = np.zeros(npts, dtype=complex)
        w_arr = np.zeros(npts)
        zoff_arr = np.zeros(npts)

        for i in range(npts):

            qap_arr[i], w_arr[i], zoff_arr[i] = horn(slen, arad, hf, wlen[i])

        return qap_arr, w_arr, zoff_arr


# Analyze system --------------------------------------------------------------

def q_output(sys_matrix, q_in):
    """
    Given the system's total transform matrix and the input beam parameter,
    this function finds the output beam parameter.

    :param np.array sys_matrix: total system matrix
    :param complex q_in: input beam parameter
    :return: output beam parameter
    """

    q_out = (sys_matrix[0, 0] * q_in + sys_matrix[0, 1]) / \
        (sys_matrix[1, 0] * q_in + sys_matrix[1, 1])

    return q_out


def beam_output(sys_matrix, q_in, wavelength):
    """
    Given the system's total transform matrix and the input beam parameter,
    this function finds the output beam parameter, waist and radius of
    curvature.

    :param np.array sys_matrix: total system matrix
    :param complex q_in: input beam parameter
    :param float wavelength: wavelength
    :return: beam parameter q, beam waist, and radius of curvature
    """

    q_out = (sys_matrix[0, 0] * q_in + sys_matrix[0, 1]) / \
        (sys_matrix[1, 0] * q_in + sys_matrix[1, 1])

    waist = np.sqrt(wavelength / (np.pi * np.imag(-1 / q_out)))
    radius_curvature = 1 / (np.real(1 / q_out))

    return q_out, waist, radius_curvature


def coupling(qin, slen, arad, hf, wavelength):
    """
    Given the systems output beam parameter q, this function will determine
    the coupling to a given horn.

    :param complex qin: input beam parameter
    :param float slen: slant length (in mm)
    :param float arad: aperture radius (in mm)
    :param float hf: horn factor
    :param float wavelength: wavelength (in mm)
    :return: coupling (some value between 0 and 1)
    """
    # TODO: Make this function work with type(wavelength)==float

    _, whorn, zoff = horn(slen, arad, hf, wavelength)

    qatwaist = np.zeros(np.alen(wavelength), dtype=complex)
    for i in range(np.alen(wavelength)):
        qatwaist[i] = q_output(freespace(zoff[i]), qin[i])

    w = np.sqrt(wavelength / (np.pi * np.imag(-1 / qatwaist)))

    r = 1 / (np.real(1 / qatwaist))

    coupling = 4 / ((((w / whorn) + (whorn / w))**2 +
                     ((np.pi * whorn * w / wavelength)**2) *
                     (((1 / r) - (1 / 1e50))**2)))

    return coupling


def edge_taper(beam_waist, aperture_radius):
    """
    Given the beam waist and an aperture radius, this function
    determines the edge taper.

    :param np.array beam_waist: beam waist
    :param float aperture_radius: aperture radius
    :return: edge taper (in dB)
    """

    taper = np.exp(-2 * aperture_radius ** 2 / beam_waist ** 2)

    taper_db = -10 * np.log10(taper)

    return taper_db


def q_from_waist(waist, wlen):
    """
    Get the beam parameter (q) from the waist. This is only
    valid for the case R=inf (i.e., at the beam waist).

    :param float waist: waist (in mm)
    :param float wlen: wavelength (in mm)
    :return: beam parameter (q)
    """

    q = 1j * np.pi * waist ** 2 / wlen

    return q
