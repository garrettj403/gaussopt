GaussOpt

*Analyze quasi-optical systems using Gaussian beam analysis*

Quasi-optical analysis is important whenever the wavelength is comparable to
the size of the optical components (making diffraction very important but not 
dominant). Gaussian beam analysis of quasi-optical systems assumes that the 
transverse amplitude profile (the E- or H-field) of the beam is similar to a 
Gaussian function. This is roughly true for beams originating from waveguide 
horn antennas.

This module uses the matrix approach to Gaussian beam transformations. 

**Author:** John Garrett (garrettj403@gmail.com)

**License:** MIT

