#!/usr/bin/env python

from distutils.core import setup  # pragma: no cover

setup(name='GaussOpt',  # pragma: no cover
      version='1.0',
      description='Gaussian beam analysis',
      author='John Garrett',
      author_email='garrettj403@gmail.com',
      license='MIT',
      py_modules=['gaussopt',],
      long_description=open('README.txt').read(),
     )